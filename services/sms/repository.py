from django.conf import settings
from urllib.request import urlopen
from urllib.error import URLError
from urllib.parse import quote


def show_debug_messages(message):
    if settings.DEBUG:
        print(message)


class SMSRepository:

    servicecodes = {
        100: "Сообщение принято к отправке. На следующих строчках вы найдете идентификаторы отправленных сообщений в том же порядке, в котором вы указали номера, на которых совершалась отправка.",
        200: "Неправильный api_id",
        201: "Не хватает средств на лицевом счету",
        202: "Неправильно указан получатель",
        203: "Нет текста сообщения",
        204: "Имя отправителя не согласовано с администрацией",
        205: "Сообщение слишком длинное (превышает 8 СМС)",
        206: "Будет превышен или уже превышен дневной лимит на отправку сообщений",
        207: "На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей",
        208: "Параметр time указан неправильно",
        209: "Вы добавили этот номер (или один из номеров) в стоп-лист",
        210: "Используется GET, где необходимо использовать POST",
        211: "Метод не найден",
        220: "Сервис временно недоступен, попробуйте чуть позже.",
        300: "Неправильный token (возможно истек срок действия, либо ваш IP изменился)",
        301: "Неправильный пароль, либо пользователь не найден",
        302: "Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)",
    }

    @staticmethod
    def send_sms(phone: str, message: str) -> int:
        sms_api_id = settings.SMS_API_ID
        sms_from = settings.SMS_FROM
        sms_time_out = settings.SMS_TIME_OUT

        show_debug_messages(f'smssend[debug] {phone}, {message}')

        url = f'http://sms.ru/sms/send?api_id={sms_api_id}&to={phone}&text={quote(message)}'
        if sms_from is not None:
            url = "%s&from=%s" % (url, sms_from)

        try:
            result = urlopen(url, timeout=sms_time_out)
            show_debug_messages(f'GET: {result.geturl()} {result.msg}')
        except URLError as errstr:
            show_debug_messages(f'smssend[debug]: {errstr}')
            raise SMSRepository.SendSMSError(f'Произошла ошибка при отправке СМС: {errstr}')

        service_result = result.read().splitlines()
        if service_result is not None and int(service_result[0]) == 100:
            show_debug_messages(f'smssend[debug]: Message send ok. ID: {service_result[1]}')

        if service_result is not None and int(service_result[0]) != 100:
            show_debug_messages(
                f'smssend[debug]: Unable send sms message to {phone} when service has returned code: {SMSRepository.servicecodes[int(service_result[0])]}')
            raise SMSRepository.SendSMSError(
                f'Произошла ошибка при отправке СМС: {SMSRepository.servicecodes[int(service_result[0])]}')

        return 0

    class SendSMSError(Exception):
        pass
