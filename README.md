## Бэк "five words"

Бэкенд и API для игры five words

### Состав:
* Django 2.*
* Стандартная админка джанги
* API на обычных Django Rest Framework
* Остальные вспомогательные библиотеки смотреть в requirements.txt

### Локальное развертывание
python3.* -m venv env
source env/bin/activate
pip install -r requirements.txt -U

создать файл .env

подтянуть переменные виртуального окружения в .env файл
пример находится в env.example
./manage.py runserver


### env.example - переменные конфигурации:
```
# режим для разработки см. документацию django
DEBUG=True

# Database configuration

POSTGRES_DB_NAME=db_name
POSTGRES_DB_USER=username
POSTGRES_DB_PASSWORD=password
POSTGRES_DB_HOST=db_address
POSTGRES_DB_PORT=5432

```

### Дополнительные требования для работы проекта:
* postgresql не ниже 9.6
* Для админки используется собственная база и модель джанги пользователей


### Docker

* Установить docker:
    https://docs.docker.com/engine/install/ubuntu/

* Установить docker-compose:
    https://docs.docker.com/compose/install/

* Запускам и запускаем контейнеры:

    * Собираем docker image (лучше выполнить команду несколько раз, с первого раза почему-то не подхватыет .env): 
    * `docker build .`

    * Создаем базу и мигруруем: 
    * `docker-compose run web python /app/manage.py migrate --noinput`

    * Создаем таблицу для кешей
    * `docker-compose run web python /app/manage.py createcachetable`

    * Создаем админа: 
    * `docker-compose run web python /app/manage.py createsuperuser`

    * Запускаем сервис:
    * `docker-compose up -d --build`

    * Для остановки сервиса 
    * `docker-compose down`

    * Очистка всего докера
    * `docker system prune`