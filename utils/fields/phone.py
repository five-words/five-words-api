import re

from django.core.validators import RegexValidator
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField

phone_regex = r"^\s*([\+\s]*7|7|8)[\s|\(|-]*[489][0-9]{2}[\s|\)|-]*[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}\s*$"

phone_regex_validator = RegexValidator(regex=phone_regex, message=u"Некорректный номер телефона")


class PhoneField(CharField):

    def __init__(self, validators=None, **kwargs):
        if validators is None:
            validators = []
        super(PhoneField, self).__init__(validators=[phone_regex_validator] + validators, **kwargs)

    def to_internal_value(self, data) -> int:
        data = super(PhoneField, self).to_internal_value(data=data)
        only_numeric_phone = list(re.sub("[^0-9]", "", data))
        if not only_numeric_phone:
            raise ValidationError('Номер телефона должен состоять из цифр')

        if only_numeric_phone[0] == '8':
            only_numeric_phone[0] = '7'
        if len(only_numeric_phone) == 10:
            only_numeric_phone.insert(0, "7")
        only_numeric_phone = "".join(only_numeric_phone)
        return int(only_numeric_phone)

    def to_representation(self, value) -> str:
        v = str(value)
        return f"+7 ({v[1:4]}) {v[4:7]}-{v[7:9]}-{v[9:11]}"
