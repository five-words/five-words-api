import logging

import six
import uuid
from rest_framework.fields import UUIDField

logger = logging.getLogger(__name__)


class NonStrictUUIDField(UUIDField):

    def to_internal_value(self, data):
        if not isinstance(data, uuid.UUID):
            if isinstance(data, six.integer_types):
                return uuid.UUID(int=data)
            elif isinstance(data, six.string_types):
                return uuid.UUID(hex=data)
            else:
                self.fail('invalid', value=data)
        return data
