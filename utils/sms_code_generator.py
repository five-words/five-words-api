import random
import string

def get_sms_code(length):
    letters = string.digits
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str
