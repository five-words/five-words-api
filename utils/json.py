from dataclasses import is_dataclass, asdict

from django.core.serializers.json import DjangoJSONEncoder


class ExtendedDjangoJSONEncoder(DjangoJSONEncoder):

    def default(self, o):
        if is_dataclass(o):
            return asdict(o)

        return super().default(o)
