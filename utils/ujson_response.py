from django.http import JsonResponse

from utils import ujson_encoder, exceptions


def _convert_error(error):
    if error is None or not error:
        return None
    if isinstance(error, exceptions.BaseError):
        return {
            'code': error.error_code,
            'message': error.message,
            'errors': error.errors
        }
    else:
        return {
            'code': 500,
            'message': 'Неизвестная ошибка',
            'errors': [str(error)]
        }


def google_json_response(api_version: str = 1, status: int = 200, data=None, error=None, meta=None, list_data=True):
    """
        Ответ сервиса в виде json в google стандарте

        :param api_version: версия апи
        :type api_version: str

        :param status: код ответа
        :type status: int

        :param data: данные
        :type data: dict or Schema

        :param error: ошибка
        :type error: None or Str or Exception

        :param meta: метаинформация (обычно тут токен пользователя для фронта)
        :type meta: None or Str or Exception
    """
    json = {
        'apiVersion': api_version,
        'data': [],
    }
    # json = {}

    if data:
        if not isinstance(data, list) and list_data:
            # в data всегда должен быть список
            data = [data]
        json['data'] = data

    # чтобы в ответе был пустой error, иначе приложение-клиент не может распарсить ответ
    json['meta'] = meta
    json['error'] = _convert_error(error)

    return JsonResponse(json, encoder=ujson_encoder.UJsonEncoder, status=status, safe=False)
