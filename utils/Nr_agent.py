import newrelic.agent


def metric(name, value):
    yield 'Custom/Herbalife-%s' % name, value


def nr_agent_metric(name, value):
    try:
        newrelic.agent.record_custom_metrics(metric(name, value))
        print(f"Логирование метрики {name} произведено")
    except Exception as e:
        print(f"Логирование метрики {name} НЕ произведено {e}")
