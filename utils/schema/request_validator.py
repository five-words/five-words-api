from marshmallow import Schema, ValidationError
from .exceptions import ValidationFail


from typing import Union


def validate_request(schema: Schema, request_body: Union[str, dict]) -> dict:
    """
        Метод для валидации данных запроса по заданной схеме.
        Нужную схему определяем во вьюшке.
        Возвращает словарь с данными или райзит ошибку валидации
    :param schema:
    :param request_body:
    :return:
    """
    validation_result = {}
    try:
        # можно передать и словарь и строку
        if isinstance(request_body, dict):
            validation_result, validation_errors = schema.load(request_body)
        else:
            validation_result, validation_errors = schema.loads(request_body)
    except ValidationError as err:
        validation_errors = err.messages

    if validation_errors:
        raise ValidationFail(validation_errors)

    return validation_result
