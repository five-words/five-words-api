from utils.exceptions import BaseError


class ValidationFail(BaseError):
    status_code = 400
    error_code = '400'
    message = 'Входные данные не соответствуют схеме'
