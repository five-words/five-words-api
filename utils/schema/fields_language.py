"""
    Перевод ошибок валидатора, который испольуется при проверках входных JSON-данных.

    Чтобы валидатор заговорил по-русски,
    надо поля наследовать от классов, определенных в этом файле.

    Например:
        Стандартное определение:
            class CalculatorApplicationFieldsSchema(Schema):
                code = fields.String(required=True)

        Определение поля с переводом:
            class CalculatorApplicationFieldsSchema(Schema):
                code = CustomStringField(required=True)
"""

from marshmallow import fields


class CustomField(fields.Field):
    """
        Общие для всех полей сообщения об ошибках
    """

    default_error_messages = {
        'required': 'Это поле обязательно для заполнения',
        'type': 'Неверный тип данных поля',
        'null': 'Поле не может содержать значения null',
        'validator_failed': 'Невалидное значение',
    }


class CustomNestedField(CustomField, fields.Nested):
    default_error_messages = {
        'type': 'Невалидный тип данных',
    }


class CustomListField(CustomField, fields.List):
    default_error_messages = {
        'invalid': 'Поле должно содержать список',
    }


class CustomStringField(CustomField, fields.String):
    default_error_messages = {
        'invalid': 'Значение должно быть строкой',
        'invalid_utf8': 'Не удалось распознать строку в utf-8'
    }


class CustomUUIDField(CustomField, fields.UUID):
    default_error_messages = {
        'invalid_uuid': 'Значение должно быть UUID.',
    }


class CustomNumberField(CustomField, fields.Number):
    default_error_messages = {
        'invalid': 'Значение должно быть числом',
        'validator_failed': 'Невалидное значение'
    }


class CustomIntegerField(CustomField, fields.Integer):
    default_error_messages = {
        'invalid': 'Значение должно быть целым числом'
    }


class CustomDecimalField(CustomField, fields.Decimal):
    default_error_messages = {
        'special': 'Специальные числовые значения не допускаются.',
    }


class CustomBooleanField(CustomField, fields.Boolean):
    default_error_messages = {
        'invalid': 'Значение должно быть true/false'
    }


class CustomFormattedString(CustomField, fields.FormattedString):
    default_error_messages = {
        'format': 'Cannot format string with given data.'
    }


class CustomDateTimeField(CustomField, fields.DateTime):
    default_error_messages = {
        'invalid': 'Значение должно быть типа datetime',
        'format': '"{input}" не может быть отформатировано в виде datetime',
    }


class CustomTimeField(CustomField, fields.Time):
    default_error_messages = {
        'invalid': 'Значение должно быть типа time',
        'format': '"{input}" не может быть отформатировано в виде time',
    }


class CustomDateField(CustomField, fields.Date):
    default_error_messages = {
        'invalid': 'Значение должно быть типа date',
        'format': '"{input}" не может быть отформатировано в виде date',
    }


class CustomTimeDeltaField(CustomField, fields.TimeDelta):
    default_error_messages = {
        'invalid': 'Значение должно быть временным интервалом',
        'format': '"{input}" не может быть отформатировано в виде timedelta',
    }


class CustomDictField(CustomField, fields.Dict):
    default_error_messages = {
        'invalid': 'Значение должно быть типа dict'
    }


class CustomURLField(CustomField, fields.URL):
    default_error_messages = {'invalid': 'Значение должно быть валидным URL'}


class CustomEmailField(CustomField, fields.Email):
    default_error_messages = {'invalid': 'Значение должно быть валидным email'}


class CustomOneOfString(CustomField, fields.String):
    default_error_messages = {'invalid': 'Должно быть одним из: {items}'}

    def __init__(self, items: list):
        self.items = items
        super(CustomOneOfString, self).__init__()

    def _deserialize(self, value, attr, data):
        if value not in self.items:
            self.fail('invalid', items=', '.join(self.items))
        else:
            return value
