from marshmallow import Schema, ValidationError
from logger import app_logger
from .exceptions import ValidationFail


from typing import Union


def validate_response(schema: Schema, data: Union[str, dict]) -> dict:
    """
        Метод для валидации данных запроса по заданной схеме.
        Нужную схему определяем во вьюшке.
        Возвращает словарь с данными или райзит ошибку валидации
    :param schema:
    :param data:
    :return:
    """
    validation_result = {}
    try:
        validation_result, validation_errors = schema.dump(data)
    except ValidationError as err:
        validation_errors = err.messages

    if validation_errors:
        [app_logger.warning(error) for error in validation_errors]
        raise ValidationFail(validation_errors)

    return validation_result
