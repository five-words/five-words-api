import datetime


def get_short_time(time: datetime.datetime) -> str:
    """Возвращает короткое время для показа"""

    return time.strftime('%H:%M %d.%m.%y')
