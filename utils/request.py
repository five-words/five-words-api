from django.http import HttpRequest


def build_absolute_uri(request: HttpRequest, location: str = None) -> str:
    # имеется ingress rule вида sdvor.com/api/brigade -> brigade-api.itlabs.io, в заголовок host
    # передается только sdvor.com и теряется /api/brigade и в итоге при попытке построить абсолютный url,
    # например на brigade-api.itlabs.io/promo_codes - получаем sdvor.com/promo_codes вместо
    # sdvor.com/api/brigade/promo_codes

    raw_host = request.get_host()
    if 'sdvor.com' in raw_host:
        return f"https://sdvor.com/api/brigade{location}"

    return request.build_absolute_uri(location=location)
