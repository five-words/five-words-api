import json
from typing import Any

from django.utils.safestring import mark_safe
from pygments import highlight
from pygments.formatters.html import HtmlFormatter
from pygments.lexers.data import JsonLexer

from utils.json import ExtendedDjangoJSONEncoder


def prettify_json(data: Any):
    data = json.dumps(data, indent=4, sort_keys=True, ensure_ascii=False, cls=ExtendedDjangoJSONEncoder).encode('utf-8')
    formatter = HtmlFormatter(style='colorful')
    highlighted_data = highlight(data, JsonLexer(), formatter)
    style = "<style>" + formatter.get_style_defs() + "</style><br>"
    return mark_safe(style + highlighted_data)
