from django.contrib.admin import RelatedFieldListFilter


class DropdownFilter(RelatedFieldListFilter):
    template = 'admin/dropdown_filter.html'
