import urllib.parse


def join(base: str, url: str):
    if not base.endswith('/'):
        base += '/'

    return urllib.parse.urljoin(base, url)
