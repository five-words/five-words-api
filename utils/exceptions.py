from typing import List, Dict
from utils.ujson_response import google_json_response


class BaseError(Exception):
    status_code = 500
    error_code = '500'
    message = 'Неизвестная ошибка'

    def __init__(self, errors: List[Dict] = None):
        if errors is None:
            self.errors = []
        else:
            self.errors = errors


class ServiceUnavailable(BaseError):
    status_code = 503
    error_code = '503'
    message = 'Сервис временно недоступен'


def error_processing(api_version):
    def decorator(fn):
        def wrapper(request, *args, **kwargs):
            try:
                return fn(request, *args, **kwargs)
            except Exception as e:
                error = e
            if isinstance(error, BaseError):
                return google_json_response(api_version=api_version, status=error.status_code, error=error)
            else:
                return google_json_response(api_version=api_version, status=500, error=error)

        return wrapper

    return decorator


def esb_event_processing(api_version):
    """
        Декоратор для событий шины ESB
        Шине нужно отвечать всегда 200, но логгировать ошибки
    :param api_version:
    :return:
    """

    def decorator(fn):
        def wrapper(request, *args, **kwargs):
            try:
                return fn(request, *args, **kwargs)
            except Exception as e:
                return google_json_response(api_version=api_version, status=200, error=e)

        return wrapper

    return decorator
