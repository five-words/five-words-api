import json

try:
    import ujson
except ImportError:
    import json as ujson


class UJsonEncoder(json.JSONEncoder):
    _encode = ujson.dumps

    inappropriate_kwargs = (
        'separators', 'skipkeys',
        'check_circular', 'allow_nan',
    )

    def __init__(self, **kw):
        for kwarg in self.inappropriate_kwargs:
            kw.pop(kwarg, None)

        super().__init__(**kw)

    def encode(self, o):
        return self._encode(
            o,
            ensure_ascii=False,
            sort_keys=self.sort_keys,
        )

    def iterencode(self, *args, **kw):
        raise NotImplementedError("ujson don't support `iterencode` interface")


UJsonEncoder.__doc__ = \
    """Be aware, ujson library don't support pass explicit `encode` (`dump(s)`) options:
    %r
    """ % (UJsonEncoder.inappropriate_kwargs, )
