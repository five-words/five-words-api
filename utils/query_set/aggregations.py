from django.db.models import Aggregate, DecimalField


class DistinctSum(Aggregate):
    function = 'SUM'
    name = 'Distinct Sum'
    template = '%(function)s(%(distinct)s%(expressions)s)'
    output_field = DecimalField()

    def __init__(self, expression, distinct=False, filter=None, **extra):
        super().__init__(
            expression, distinct='DISTINCT ' if distinct else '',
            filter=filter, **extra
        )
