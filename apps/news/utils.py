def upload_path_handler(instance, filename: str) -> str:
    return f'newsimages/{instance.news.id}/{filename}'
