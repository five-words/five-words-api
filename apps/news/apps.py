from django.apps import AppConfig


class NewsAppConfig(AppConfig):
    name = 'apps.news'
    verbose_name = 'Новости'

