from django.contrib.auth.models import AnonymousUser
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins, filters
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from .models import News, NewsImage
from .serializers import NewsSerializer

class NewsViewSet(
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    permission_classes = []
    serializer_class = NewsSerializer
    queryset = News.objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title')
