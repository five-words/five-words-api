import logging

from rest_framework import serializers
from rest_framework.fields import EmailField, CharField
from rest_framework.validators import UniqueValidator

from utils.fields.phone import PhoneField
from utils.serializers import ValidatingSerializer
from .models import News, NewsImage


class NewsImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsImage
        fields = '__all__'


class NewsSerializer(serializers.ModelSerializer):

    images = NewsImageSerializer(many=True, read_only=True)

    class Meta:
        model = News
        fields = ['id', 'title', 'text', 'created_at', 'images']
