from typing import Optional, List

from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.conf import settings
from django.db.models import Count
from kiss_cache import Cache

from apps.barlondonapp.models import Client
from apps.user.models import User


IN_MEMORY_CACHE = Cache()


class BarlondonAppRepository:

    @staticmethod
    def get_client_by_phone(phone: int) -> Optional[Client]:
        try:
            return Client.objects.get(phone=str(phone))
        except Client.DoesNotExist:
            return None

    @staticmethod
    @IN_MEMORY_CACHE.memoize(expire=60 * 5)
    def is_client_exists(*, user_id: int) -> bool:
        return Client.objects.filter(id=user_id, is_active=True).exists()

    @staticmethod
    def get_client_by_code(client_referrer_code: str) -> Client:
        try:
            return Client.objects.filter(client_referrer_code=client_referrer_code).get()
        except Client.DoesNotExist:
            raise BarlondonAppRepository.ClientNotFoundByReferrerCode()

    @staticmethod
    def get_client_by_secret(client_secret: str) -> Client:
        try:
            return Client.objects.filter(client_secret=client_secret).get()
        except Client.DoesNotExist:
            raise BarlondonAppRepository.ClientNotFoundBySecret()

    @staticmethod
    def is_client_code_valide(client_referrer_code: str) -> bool:
        client = BarlondonAppRepository.get_client_by_code(client_referrer_code)

        try:
            if client.auth_user.is_staff:
                return True
        except Client.DoesNotExist:
            return True

        client_code_activations_left = BarlondonAppRepository.client_code_activations_left(client)

        if client_code_activations_left <= 0:
            raise BarlondonAppRepository.ReferrerCodeActivationsCountExceed()

        return True

    @staticmethod
    def is_client_secret_valide(client_secret: str) -> bool:
        client = BarlondonAppRepository.get_client_by_secret(client_secret)
        return True

    @staticmethod
    def client_code_activations_left(client: Client) -> int:
        try:
            activations_count = client.referrers.count()
            return settings.MAX_PROMOCODE_ACTIVATION - activations_count
        except Client.DoesNotExist:
            return settings.MAX_PROMOCODE_ACTIVATION

    @staticmethod
    def activate_user(user: User, client_referrer_code: str) -> None:
        # Получаем клиент-рефферала
        try:
            client_refferer = BarlondonAppRepository.get_client_by_code(client_referrer_code=client_referrer_code)
        except Client.DoesNotExist:
            raise BarlondonAppRepository.ClientNotFoundByReferrerCode()

        new_client = Client(auth_user=user, phone=user.phone, is_active=True, referrer=client_refferer)
        user.refferer_client_code = client_referrer_code
        user.save()
        new_client.save()

    class ClientNotFoundByReferrerCode(Exception):
        pass

    class ClientNotFoundBySecret(Exception):
        pass

    class ReferrerCodeActivationsCountExceed(Exception):
        pass
