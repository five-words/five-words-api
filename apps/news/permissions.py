from rest_framework.permissions import BasePermission

from apps.barlondonapp.models import Client
from apps.barlondonapp.repository import BarlondonAppRepository
from apps.user.models import User


class IsStaffOrAdmin(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        return user and not user.is_anonymous and user.is_active and (user.is_staff or user.is_admin)
