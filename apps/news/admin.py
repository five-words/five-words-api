from typing import List

from django.contrib import admin, messages
from import_export.admin import ExportMixin

from utils.fields import PhoneField
from utils.admin.formatting import prettify_json
from .models import News, NewsImage


class NewsImageInlineAdmin(admin.TabularInline):
    model = NewsImage

    extra = 0

    def has_add_permission(self, request, obj):
        return True

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return True


@admin.register(News)
class ProfileQuestionAdmin(ExportMixin, admin.ModelAdmin):
    inlines = [
        NewsImageInlineAdmin,
    ]

    list_display = ('title', 'text', 'created_at')

    search_fields = ['title', 'text', 'created_at']
    ordering = ['created_at']

    def get_queryset(self, request):
        queryset = super().get_queryset(request=request)
        queryset = queryset.prefetch_related("images")
        return queryset
