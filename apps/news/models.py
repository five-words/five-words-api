from django.contrib.humanize.templatetags.humanize import naturaltime, naturalday
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.deletion import CASCADE, SET_DEFAULT, SET_NULL
from django.db.models.signals import pre_save
from django.dispatch import receiver
from pydantic import BaseModel
from apps.user.models import User

from utils.fields.phone import PhoneField
from utils.secret_generator import get_random_string
from .utils import upload_path_handler
from .validators import validate_file_extension


class News(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Создан")
    title = models.CharField('Заголовок новости', max_length=255)
    text = models.TextField('Текст новости', blank = True)

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = 'новость'
        verbose_name_plural = 'новости'


class NewsImage(models.Model):
    news = models.ForeignKey(News, verbose_name="Новость", blank=False, on_delete=models.CASCADE,
                             related_name='images', related_query_name='images_query')
    image_title = models.CharField('Заголовок картинки', blank=True, max_length=255)
    file = models.FileField('Файл', upload_to=upload_path_handler, validators=[validate_file_extension])

    main_image = models.BooleanField('Основное изображение')

    def __str__(self):
        return self.image_title

    class Meta:
        verbose_name = 'изображения новости'
        verbose_name_plural = 'изображения новости'
