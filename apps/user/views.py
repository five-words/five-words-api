from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotAuthenticated, MethodNotAllowed, APIException
from rest_framework.response import Response

from .serializers import UserSerializer, UserActivateSerializer
from .models import User


class UserViewSet(viewsets.GenericViewSet):
    serializer_class = UserActivateSerializer

    def create(self, request):
        pass

    @action(detail=False, methods=['GET'])
    def current(self, *args, **kwargs):

        if self.request.method != 'GET':
            raise MethodNotAllowed('Разрешен только GET метод')

        user = self.request.user
        if isinstance(user, User):
            data = UserSerializer(data=self.request.data, context={
                                  'request': self.request}).to_representation(instance=user)
            return Response(data=data)

        raise NotAuthenticated('Не авторизован')
