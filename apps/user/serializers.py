from rest_framework import fields
from rest_framework.exceptions import ValidationError
from rest_framework import serializers

from apps.auth.validators import IsActiveClientPhone
from .models import User
from utils.fields.phone import PhoneField
from utils.serializers import ValidatingSerializer


class CreateUserSerializer(ValidatingSerializer):
    phone = PhoneField(required=True, validators=[IsActiveClientPhone()])
    code = fields.CharField(required=True)


class RetrieveUserSerializer(ValidatingSerializer):
    id = fields.IntegerField(source="id")
    phone = PhoneField()


class UserSerializer(serializers.ModelSerializer):
    activated = serializers.SerializerMethodField(source='get_activated')

    def get_activated(self, obj):
        return obj.activated

    class Meta:
        model = User
        fields = '__all__'


class UserActivateSerializer(ValidatingSerializer):
    code = fields.CharField(required=True)
