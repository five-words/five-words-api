from datetime import datetime
from datetime import timedelta

import jwt
from django.conf import settings
from django.db import models
from django.core.exceptions import ObjectDoesNotExist


class User(models.Model):

    phone = models.BigIntegerField(
        unique=True,
        blank=False
    )

    is_staff = models.BooleanField(default=False)

    is_admin = models.BooleanField(default=False)

    is_active = models.BooleanField(default=True)

    is_anonymous = False

    # Свойство `USERNAME_FIELD` сообщает нам, какое поле мы будем использовать для входа.
    USERNAME_FIELD = 'phone'

    REQUIRED_FIELDS = ('phone',)

    def __str__(self):
        return f'{str(self.phone)}'

    @property
    def token(self):
        return self._generate_jwt_token()

    @property
    def activated(self):
        try:
            return not self.client is None and self.client.activated
        except ObjectDoesNotExist:
            return False

    def get_full_name(self):
        return self.phone

    def get_short_name(self):
        return self.phone

    def _generate_jwt_token(self):
        dt = datetime.now() + timedelta(days=90)

        token = jwt.encode({
            'id': self.pk,
            'phone': self.phone,
            'exp': int(dt.strftime('%s'))
        }, settings.JWT_SECRET, algorithm='HS256')

        return token.decode('utf-8')

    class Meta:
        verbose_name = 'Авторизованный пользователь'
        verbose_name_plural = 'Авторизованные пользователи'
        indexes = [
            models.Index(fields=['phone'])
        ]
