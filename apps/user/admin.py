from django.contrib import admin

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('phone', 'id', 'is_staff', 'is_active')

    def has_delete_permission(self, request, obj=None):
        return False
