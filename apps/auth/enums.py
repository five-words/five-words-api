from enum import Enum


class AuthCookie(Enum):
    JWT_AUTH_TOKEN = "jwt_auth_token"
    FIRST_STEP_SIGN_IN_PASSED = "first_step_sign_in_passed"


class CacheKey(Enum):
    @staticmethod
    def get_cache_key_for_two_factor_authorization(x: int):
        return f"two_factor_authorization_validated_{x}"
