from rest_framework import fields
from rest_framework.exceptions import ValidationError

from apps.auth.validators import IsClientPhone, IsActiveClientPhone, IsPhoneValid
from utils.fields.phone import PhoneField
from utils.serializers import ValidatingSerializer


class SendSMSFromInviteSerializer(ValidatingSerializer):
    phone = PhoneField(required=True, validators=[IsPhoneValid()])


class SignInSerializer(ValidatingSerializer):
    phone = PhoneField(required=True, validators=[IsPhoneValid()])
    code = fields.CharField(required=True)
