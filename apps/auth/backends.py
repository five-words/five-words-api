from abc import ABC

import jwt
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, get_authorization_header

from apps.user.models import User


class JWTBaseAuthentication(BaseAuthentication, ABC):
    def get_token(self, request):
        raise NotImplementedError("Реализуйте данный метод в своем подклассе")

    def authenticate(self, request):
        return self.authenticate_credentials(token=self.get_token(request=request))

    def authenticate_credentials(self, token):

        if not token:
            return None

        if isinstance(token, bytes):
            token = token.decode('utf-8')

        secret = getattr(settings, 'JWT_SECRET', None)
        if secret is None:
            raise ImproperlyConfigured("settings.JWT_SECRET не сконфигурирован.")

        try:
            decoded_token = jwt.decode(token, secret, algorithms=['HS256'])
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed("Не удалось корректно декодировать токен пользователя")

        try:
            return User.objects.get(phone=decoded_token.get('phone')), None
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed("Пользователь не найден")

        return None, None


class JWTHeaderAuthentication(JWTBaseAuthentication):
    www_authenticate_realm = 'JWT'

    def get_token(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != b'jwt':
            return None

        if len(auth) == 1:
            raise exceptions.AuthenticationFailed("Некорректный заголовок. Учетные данные не были предоставлены")
        elif len(auth) > 2:
            raise exceptions.AuthenticationFailed("Некорректный заголовок. Учетные данные не должны содержать пробелов")

        return auth[1]
