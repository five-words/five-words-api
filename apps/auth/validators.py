from django.core.cache import cache
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField

from apps.auth.enums import CacheKey
from utils.fields.phone import PhoneField


class IsPhoneValid:
    def __call__(self, value):
        phone = PhoneField().to_internal_value(value)

class IsClientPhone:
    def __call__(self, value):
        phone = PhoneField().to_internal_value(value)


class IsActiveClientPhone:
    def __call__(self, value):
        phone = PhoneField().to_internal_value(value)

        # if client is None:
        #     raise ValidationError("Пользователь с таким номером телефона не зарегистрирован")

        # if client.is_active is False:
        #     raise ValidationError("Пользователь еще не активен в системе")


class IsPromocodeActive:
    def __call__(self, value):
        client_secret = CharField().to_internal_value(value)

        # if code_is_valid is None:
        #     raise ValidationError("Код не действителен")


class IsFirstStepPassed:
    def __call__(self, value):
        phone = PhoneField().to_internal_value(value)
        if not cache.get(CacheKey.get_cache_key_for_two_factor_authorization(phone)):
            raise ValidationError("Необходимо пройти первый этап авторизации")
