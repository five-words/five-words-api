from datetime import datetime, timedelta

from django.core.cache import cache
from django.conf import settings
from rest_framework import viewsets
from rest_framework.exceptions import AuthenticationFailed, MethodNotAllowed, APIException
from rest_framework.response import Response

from apps.auth.serializers import SendSMSFromInviteSerializer, SignInSerializer
from apps.user.models import User
from services.sms.repository import SMSRepository
from utils.sms_code_generator import get_sms_code


class SendSMSFromInviteViewSet(viewsets.GenericViewSet):
    serializer_class = SendSMSFromInviteSerializer
    http_method_names = ['post']

    def create(self, request):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        phone: int = serializer.validated_data.get('phone')

        cache_key = str(phone)
        cache_value = cache.get(cache_key)

        if cache_key == settings.TEMP_PHONE:
            code = str(settings.TEMP_TOKEN)
            cache.set(cache_key, {'code': code, 'resend_expire_in': datetime.now(
                tz=None) + timedelta(seconds=settings.SMS_AUTH_RESEND_EXPIRE_IN_SECONDS)})
            return Response(status=200, data=f'СМС отправлена{" %s" % settings.TEMP_TOKEN if settings.DEBUG else ""}')

        if (cache_value is None) or (cache_value.get('resend_expire_in') < datetime.now(tz=None)):
            code = get_sms_code(settings.SMS_AUTH_CODE_LENGTH)
            cache.set(cache_key, {'code': code, 'resend_expire_in': datetime.now(
                tz=None) + timedelta(seconds=settings.SMS_AUTH_RESEND_EXPIRE_IN_SECONDS)})

            if SMSRepository.send_sms(phone=str(phone), message=settings.SMS_AUTH_TEXT % code) != 0:
                raise APIException('Ошибка отправки СМС, попробуйте повторить запрос позже', 500)
            else:
                return Response(status=200, data=f'СМС отправлена{" %s" % code if settings.DEBUG else ""}')
        else:
            raise MethodNotAllowed('СМС уже отправлена, повторите попытку позже')


class SignInViewSet(viewsets.GenericViewSet):
    serializer_class = SignInSerializer
    http_method_names = ['post']

    def create(self, request):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        phone: int = serializer.validated_data.get('phone')
        code: str = serializer.validated_data.get('code')

        cache_key = str(phone)
        cache_value = cache.get(cache_key)

        if not cache_value is None and cache_value.get('code') == code:
            response = Response(status=200)
            user, created = User.objects.get_or_create(phone=phone)
            if created:
              
                user.save()
            response = Response(status=200, data={"token": user.token, "current_user_id": user.id})
            return response
        else:
            raise AuthenticationFailed('Проверочный код неверный')
